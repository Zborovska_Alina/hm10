﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformers
{
    class WaterTransformer : BaseTransformer
    {
        public WaterTransformer(string name, Field field) : base(name, field)
        {
        }

        public override void Move(int speed)
        {
            if (IsHumanoid)
            {
                Console.WriteLine($"{Name} runs at speed {speed}");
            }
            else
            {
                Console.WriteLine($"{Name} swims at speed {speed}");
            }
            base.Move(speed);
        }
    }
}
